$(function () {
  var CONST = {
    $document: $(document),
    $window: $(window),
    $body: $('body'),
    $header: $('#header'),
    $headerInner: $('#header-inner'),
  };



  //Переключение меню
  var $menuToggle = $('#menu-toggle');

  $menuToggle.click(function () {
    $(this).toggleClass('header__menu-toggle--active');
    CONST.$header.toggleClass('header--menu-opened');
  });



  //Прилипающий хедер
  CONST.$header.removeClass('header--before-dom-ready');

  if (CONST.$document.scrollTop() > 80) {
    CONST.$header.addClass('header--fixed');
    CONST.$headerInner.addClass('header__inner--fixed');
  }

  CONST.$document.scroll(function () {
    var $this = $(this);

    if ($this.scrollTop() > 80) {
      CONST.$header.addClass('header--fixed');
      CONST.$headerInner.addClass('header__inner--fixed');
      $searchForm.trigger('close');
    }
    else {
      CONST.$header.removeClass('header--fixed');
      CONST.$headerInner.removeClass('header__inner--fixed');
    }
  })


  //Поиск по сайту
  var $searchToggle = $('#search-toggle'),
    $searchForm = $('#search-form'),
    $searchInput = $searchForm.find('input'),
    isClickedToggle = false;

  $searchForm.on('open', function () {
    CONST.$headerInner.addClass('header__inner--search-opened');
    $searchToggle.addClass('header__search-toggle--active');
    $searchForm.addClass('header__search--open');
    setTimeout(function () {
      $searchInput.focus();
    }, 305);
    isClickedToggle = true;
  });

  $searchForm.on('close', function () {
    CONST.$headerInner.removeClass('header__inner--search-opened');
    $searchToggle.removeClass('header__search-toggle--active');
    $searchForm.removeClass('header__search--open');
    isClickedToggle = false;
  });

  $searchToggle
    .click(function () {
      var inputValue = $searchInput.val().trim();

      if (isClickedToggle === false && inputValue === '') {
        $searchForm.trigger('open');
      }
      else if (isClickedToggle === true && inputValue === '') {
        $searchForm.trigger('close');
        $searchInput.val('');
      }
      else {
        $searchForm.submit();
      }
      //===
      $sidebar.trigger('close');
      openedByUser = false;
      closedByUser = true;
      //===
    })
    .mouseup(function (e) {
      e.stopPropagation();
    });

  CONST.$document.mouseup(function (e) {
    if (
      !$searchForm.is(e.target) && 
      $searchForm.has(e.target).length === 0 &&
      $searchInput.val().trim() === ''
    ) {
      $searchForm.trigger('close');
      $searchInput.val('');
    }
  });



  //Появление блоков
  (function () {
    var $animatedBlocks = $('.animate'),
      windowHeight = CONST.$window.height();
  
    $animatedBlocks.each(function (ind, el) {
      var $this = $(el);
  
      if (CONST.$document.scrollTop() > $this.offset().top - windowHeight + 300) {
        $this.addClass('animate--active');
      }
    });
  
    CONST.$document.scroll(function () {
       $animatedBlocks.each(function (ind, el) {
         var $this = $(el);
  
         if (CONST.$document.scrollTop() > $this.offset().top - windowHeight + 300) {
           $this.addClass('animate--active');
         }
       });
    })
  })();



  //Сайдбар
  if (window.pageHasSidebar === true) {
    var $sidebar = $('#sidebar'),
      $pageContent = $('.page-content'),
      $sidebarToggle = $('#sidebar-toggle'),
      openedByUser = false,
      closedByUser = false;

    $sidebar.on('open', function () {
      $sidebarToggle.addClass('sidebar-toggle--active');
      $sidebar.addClass('sidebar--open');
      CONST.$body.addClass('sidebar-opened');
      $pageContent.addClass('page-content--opened-sidebar');
    });

    $sidebar.on('close', function () {
      $sidebarToggle.removeClass('sidebar-toggle--active');
      $sidebar.removeClass('sidebar--open');
      CONST.$body.removeClass('sidebar-opened');
      $pageContent.removeClass('page-content--opened-sidebar');
    });
      
    $sidebarToggle
      .click(function () {
        var $this = $(this);

        if ($this.hasClass('sidebar-toggle--active') === false) {
          $sidebar.trigger('open');
          openedByUser = true;
        }
        else if ($this.hasClass('sidebar-toggle--active') === true) {
          $sidebar.trigger('close');
          openedByUser = false;
          closedByUser = true;
        }
      })
      .mouseup(function (e) {
        e.stopPropagation();
      });

    CONST.$document.mouseup(function (e) {
      if (
        !$sidebar.is(e.target) && 
        $sidebar.has(e.target).length === 0
      ) {
        $sidebar.trigger('close');
        openedByUser = false;
        closedByUser = true;
      }
    });
  
    if (isMobile() === false) {
      CONST.$document.scroll(function () {
        var $this = $(this);
    
        if (
          $this.scrollTop() > CONST.$window.height() / 2 && 
          openedByUser === false &&
          closedByUser === false
        ) {
          $sidebar.trigger('open');
        }
        else if (openedByUser === false) {
          $sidebar.trigger('close');
        }
      })
    }
  }



  //Вкладки слайдеров
  var $tabToggle = $('.js-title-tab-toggle'),
    $tab = $('.js-slider-tab');

  $tabToggle.click(function () {
    var $this = $(this),
      index = $this.index();

    $tabToggle.removeClass('title-main__item--active');
    $tab.addClass('slider--hidden-accessible');

    $this.addClass('title-main__item--active');
    $tab.eq(index).removeClass('slider--hidden-accessible');
  })



  //Описание по ховеру на слайде
  var $hoverSlide = $('.js-show-slider-text');

  if (isMobile()) {
    var isClicked = false,
      $sliderText = $hoverSlide.find('.slider__item-text'),
      $sliderLink = $hoverSlide.find('.slider__item-content-link'),
      $sliderIcon = $hoverSlide.find('.slider__item-content-icon'),
      $sliderTextIcon = $hoverSlide.find('.slider__item-content-text-icon'),
      $sliderTitle = $hoverSlide.find('.slider__item-content-title');

    $hoverSlide
      .on('slide:show', function () {
        $sliderText.slideDown();
        $sliderLink.css('opacity', '1');
        $sliderIcon.css('opacity', '0');
        $sliderTextIcon.css('opacity', '0');
        $sliderTitle.fadeOut();
        isClicked = true;
      })
      .on('slide:hide', function () {
        $sliderText.slideUp();
        $sliderLink.css('opacity', '0');
        $sliderIcon.css('opacity', '1');
        $sliderTextIcon.css('opacity', '1');
        $sliderTitle.fadeIn();
        isClicked = false;
      });

    $hoverSlide.click(function () {
      if (isClicked) {
        $hoverSlide.trigger('slide:hide');
      }
      else {
        $hoverSlide.trigger('slide:show');
      }
    });

    CONST.$document.on('mouseup', function (e) {
      if (
        !$hoverSlide.is(e.target) && 
        $hoverSlide.has(e.target).length === 0 &&
        !$('.slick-arrow').is(e.target) &&
        $('.slick-arrow').has(e.target).length === 0
      ) {
        $hoverSlide.trigger('slide:hide');
      }
    });
  }
  else {
    $hoverSlide
      .mouseenter(function () {
        var $this = $(this),
          $sliderText = $(this).find('.slider__item-text'),
          $sliderTitle = $(this).find('.slider__item-content-title');
  
        $sliderText.slideDown();
        $sliderTitle.fadeOut()
      })
      .mouseleave(function () {
        var $this = $(this),
          $sliderText = $(this).find('.slider__item-text'),
          $sliderTitle = $(this).find('.slider__item-content-title');
  
        $sliderText.slideUp();
        $sliderTitle.fadeIn()
      });
  }




  //Фон на главной странице
  (function () {
    var $bg = $('#index-bg'),
      scrollRange = CONST.$window.outerHeight() / 2,
      blurFactor = 12,
      scrollOffset = 5;
  
    if (
      CONST.$document.scrollTop() > scrollOffset && 
      CONST.$document.scrollTop() < scrollRange + scrollOffset
    ) {
      var blurVal = CONST.$document.scrollTop() * (blurFactor / scrollRange);
      $bg.css('filter', 'blur(' + blurVal + 'px)');
    } 
    else if (CONST.$document.scrollTop() > scrollRange + scrollOffset) {
      $bg.css('filter', 'blur(' + blurFactor + 'px)');
    };
  
    CONST.$document.scroll(function () {
      var $this = $(this);
  
      if ($this.scrollTop() <= scrollOffset) {
        $bg.css('filter', 'blur(0)');
      }
      else if (
        $this.scrollTop() > scrollOffset && 
        $this.scrollTop() < scrollRange + scrollOffset
      ) {
        var blurVal = $this.scrollTop() * (blurFactor / scrollRange);
        $bg.css('filter', 'blur(' + blurVal + 'px)');
      }
      else {
        $bg.css('filter', 'blur(' + blurFactor + 'px)');
      }
    })
  })();



  //Затемнение верха страниц
  (function () {
    var $pageBg = $('#page-bg'),
      scrollRange = CONST.$window.outerHeight() / 2,
      scrollOffset = 5;
  
    if (
      CONST.$document.scrollTop() > scrollOffset && 
      CONST.$document.scrollTop() < scrollRange + scrollOffset
    ) {
      var opacityValue = 1 - CONST.$document.scrollTop() * (1 / scrollRange);
      $pageBg.css('opacity', opacityValue);
    }
    else if (CONST.$document.scrollTop() > scrollRange + scrollOffset) {
      $pageBg.css('opacity', 0);
    }
  
    CONST.$document.scroll(function () {
      var $this = $(this);
  
      if ($this.scrollTop() <= scrollOffset) {
        $pageBg.css('opacity', 1);
      }
      else if (
        $this.scrollTop() > scrollOffset && 
        $this.scrollTop() < scrollRange + scrollOffset
      ) {
        var opacityValue = 1 - $this.scrollTop() * (1 / scrollRange);
        $pageBg.css('opacity', opacityValue);
      }
      else {
        $pageBg.css('opacity', 0);
      }
    });
  })();



  //Попап обратной связи
  $('#callback-popup-open').click(function () {
    $('#callback-popup').addClass('popup--open');
  });



  //Закрытие попапа
  $('.popup__close').click(function () {
    $(this).closest('.popup').removeClass('popup--open');
  });

  var $popupContent = $('.popup__inner');

  CONST.$document.mouseup(function (e) {
    if (
      !$popupContent.is(e.target) && $popupContent.has(e.target).length === 0 ||
      $('.popup__close').is(e.target)
    ) {
      $('.popup').removeClass('popup--open');
    }
  });



  //Отправка формы
  $('.js-callback-form').submit(function (e) {
    e.preventDefault();
    var $form = $(this);

    if (window.utils.validateForm($form)) {
      $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function (data) {
          window.utils.notification('Сообщение успешно отправлено', 4000);
          $('#callback-popup').removeClass('popup--open');
          $form.find('input , textarea').not('input:hidden, input:submit').val('');
        }
      });
      return false;
    }
    else {
      window.utils.notification('Проверьте правильность введенных данных', 3000);
    }
  });



  //Очистка уведомлений формы
  $('form').on('focus', 'input:not([type="checkbox"]), input:not([type="radio"]), textarea', function () {
    $(this).closest('.form__field-wrap').find('.form__field-alert').remove();
  })



  //Пользовательское соглашение
  var $agreement = $('.agreement'),
    $agreementCheck = $agreement.find('.agreement__checkbox'),
    $formSubmit = $agreement.closest('form').find('.btn');

  $agreementCheck.click(function () {
    $(this).prop('checked') === true ?
      $formSubmit.prop('disabled', false) :
      $formSubmit.prop('disabled', true);
  })
})



function isMobile() {
  if (navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  } 
  else {
    return false;
  }
}

window.utils = {
  notification: function (message, duration) {
    var $new_message = $('<div class="notification"></div>'),
      $opened_messages = $('.notification');

    $new_message.html(message);

    if ($opened_messages.length) {
      var $last_message = $opened_messages.last(),
        top_offset = $last_message[0].offsetTop + $last_message.outerHeight() + 15;

      $new_message.css('top', top_offset + 'px')
    }

    $new_message.appendTo('body');
    $new_message.css('opacity'); //reflow hack for transition
    $new_message.addClass('notification--show');

    if (duration) {
      setTimeout(function () {
        $new_message.fadeOut(function () {
          $new_message.remove();
        })
      }, duration);
    }

    $('.notification').click(function () {
      $(this).fadeOut(function () {
        $new_message.remove();
      })
    });
  },

  validateForm: function ($form) {
    $form.find('.form__field-alert').remove();

    function showAlert(message) {
      return $('<div class="form__field-alert">').html(message);
    }

    function checkRequiredField(field) {
      if (field.value) {
        return true;
      } 
      else {
        showAlert('Обязательное поле').insertBefore(field);
        return false;
      }
    }

    function checkNumericField(field) {
      var val = field.value,
        regexp = /^[^a-zA-Z]*$/g;

      if (val !== '' && val.match(regexp)) {
        return true;
      } 
      else {
        showAlert('Введите корректное значение').insertBefore(field);
        return false;
      }
    }

    function checkEmailField(field) {
      var val = field.value,
        regexp = /^[0-9a-zА-Яа-я\-\_\.]+\@[0-9a-zА-Яа-я-]{2,}\.[a-zА-Яа-я]{2,}$/i;

      if (val !== '' && val.match(regexp)) {
        return true;
      } 
      else {
        showAlert('Введите корректный адрес').insertBefore(field);
        return false;
      }
    }

    function validateField(field) {
      if ($(field).hasClass('js-required')) {
        return checkRequiredField(field);
      }
      else if ($(field).hasClass('js-required-email')) {
        return checkEmailField(field);
      } 
      else if ($(field).hasClass('js-required-numeric')) {
        return checkNumericField(field);
      } 
      else {
        return true;
      }
    }

    var fields = $form.find('input, textarea'),
      isFormValid = true;

    $.each(fields, function (ind, el) {
      var checkedField = validateField(el);
      isFormValid = isFormValid && checkedField;
    });

    return isFormValid;
  }
}
